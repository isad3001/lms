var globalRequestID = null //stores the request sent when user modifies a request
const workingHours = {
  "start": new Date('01-01-21 08:00:00'),
  "end": new Date('01-01-21 18:00:00')
}

document.addEventListener("DOMContentLoaded", () => {
  genderChecker()
  hidingMessages()
  remaining_leaves('/leaves', 'My Leaves')
  $('#counter').hide()
  $('.time-container').hide()
  $('#employee-leaves-container').empty()
  $('#submit-add-employee').click((event) => {
    // prevents the default action of the form  
    event.preventDefault()
    hidingMessages()
    addEmployeeManager()
  })

  // trim() will remove the line feed from the string
  if ($.trim($('#current-role').html()) === 'Head of Department' || 
      $.trim($('#current-role').html()) === 'CEO' ||
      $.trim($('#current-department').html()) === 'People and Culture' ||
      $.trim($('#delegate-status').html()) === 'Active'){   
    requests()
  }
  if ($.trim($('#current-department').html()).localeCompare('People and Culture') == 0 || 
      $.trim($('#current-role').html()).localeCompare('CEO') == 0){   
    $('#nav-add-employee').click(() => {
      hidingMessages()
    })
    $('#nav-add-department').click(() => {
      hidingMessages()
    })
    $('#nav-add-leave').click(() => {
      hidingMessages()
    })
    $('#nav-add-study-period').click(() => {
      hidingMessages()
    })

    $('#nav-add-leave').click(() => {
      hidingMessages()
      addLeaveManager()
    })
  }

  $('#nav-dashboard').click(() => {
    $('#employees-list-container').empty()
    $('#employee-leaves-container').empty()
    $('#past-leaves-container').empty()
    $('#past-requests-container').empty()
    $('#pending-requests-container').empty()
    $('#holidays-container').empty()
    remaining_leaves('/leaves', 'My Leaves')

    if ($.trim($('#current-role').html()) === 'Head of Department' || 
        $.trim($('#delegate-status').html()) === 'Active' ||
        $.trim($('#current-role').html()) === 'CEO'){   
      requests()
    }
  })
  $('#calc').click(() => {
    $('#counter').show()
  })

  $('#nav-add-department').click(() => {
    hidingMessages()
  })

  $('#nav-add-leave').click(() => {
    hidingMessages()
  })

  $('#nav-add-study-period').click(() => {
    hidingMessages()
  })
  
  if($.trim($('#current-role').html()).localeCompare('Head of Department') == 0 ||
     $.trim($('#current-role').html()).localeCompare('CEO') == 0){
    $('#nav-view-employees').click(() => {
      $('#requests-container').empty()
      $('#employee-leaves-container').empty()
      $('#past-leaves-container').empty()
      $('#past-requests-container').empty()
      $('#pending-requests-container').empty()
      $('#holidays-container').empty()
      hidingMessages()
      employeeList()
    })
  }

  $('#my-leaves').click(() => {      
    $('#employees-list-container').empty()
    $('#requests-container').empty()
    $('#past-leaves-container').empty()
    $('#past-requests-container').empty()
    $('#pending-requests-container').empty()
    $('#holidays-container').empty()
    remaining_leaves('/leaves', 'My Leaves')
  })

  $('#nav-view-past-leaves').click(() => {   
    $('employee-list-container').empty()   
    $('#requests-container').empty()
    $('#past-leaves-container').empty()
    $('#past-requests-container').empty()
    $('#employee-leaves-container').empty()
    $('#pending-requests-container').empty()
    $('#holidays-container').empty()
    
    pastLeaves('/pastLeaves', 'My Past Leaves')
  })

  $('#nav-view-past-requests').click(() => {      
    $('#past-leaves-container').empty()
    $('#past-requests-container').empty()
    $('#requests-container').empty()
    $('#employee-leaves-container').empty()
    $('#pending-requests-container').empty()
    $('#holidays-container').empty()
    pastRequests("My Past Requests")
  })

  $('#nav-view-pending-requests').click(() => {      
    $('#past-leaves-container').empty()
    $('#past-requests-container').empty()
    $('#requests-container').empty()
    $('#employee-leaves-container').empty()
    $('#pending-requests-container').empty()
    $('#holidays-container').empty()
    pendingRequests("My Pending Requests")
  })

  $('#nav-view-holidays').click(() => {      
    $('#past-leaves-container').empty()
    $('#past-requests-container').empty()
    $('#holidays-container').empty()
    $('#requests-container').empty()
    $('#employee-leaves-container').empty()
    $('#pending-requests-container').empty()
    view_holidays()
  })

  $('#nav-add-holiday').click(() => {       
    $('#adding-holiday-message').hide()
    
  })

  $('#nav-delegate').click(() => {
    delegateEmployeeHandler()
  })

  $('#nav-request-leave').click(() => {
    hidingMessages()
    $('#form-request-leave').hide()
    leaveManager()
  })

  $('#submit-add-department').click((event) => {
    event.preventDefault()
    hidingMessages()
    addDepartment()
  })

  $('#submit-add-study-period').click((event) => {
    event.preventDefault()
    hidingMessages()
    addStudyPeriod()
  })

  $('#submit-add-leave').click((event) => {
    event.preventDefault()
    addLeave()
  })
  $('#submit-remove-leave').click((event) => {
    event.preventDefault()
    removeLeave()
  })

  $('#submit-add-holiday').click((event) => {
    event.preventDefault()
    addHoliday()
  })

  $('#submit-request-leave').click((event) => {
    event.preventDefault()
    submitLeaveRequest(globalRequestID)
  })
    
  $('#submit-gender').click(() => {
    hidingMessages()
    genderSubmit()
  })

  $('#nav-carry-forward-leaves').click(() => {      
    $('#past-leaves-container').empty()
    $('#past-requests-container').empty()
    $('#pending-requests-container').empty()

    carryForwardLeavesHandler()
    $('#carryForwardLeaveModal').toggle('show')
  })
})

/* 
  This function runs everytime the page is loaded. If the gender is not saved, it will prompt only once to the user to
  select the gender. This prompt can't be closed and is mandatory to fill. 
  */
function genderChecker(){
  fetch('/employee')
  .then(response => response.json())
  .then(employee => {
    if ((employee.gender).length == 0){
      $('#genderModal').modal('show')
    }
  })
  .catch(error => {
    console.log(error.response)
  })
}

function genderSubmit(){
  const gender_modal_body = $('#gender-modal-body')
  const submitting = $('<div/>')
  .attr({'class': 'alert alert-success container mb-3', 'role': 'alert'})
  submitting.text('Saving gender...')
  submitting.insertAfter(gender_modal_body)
  const gender = $('#employee-gender-m').val()
  fetch('/employee', {
    method: 'PUT',
    body: JSON.stringify({
      "gender": gender,
    }),
    credentials: 'same-origin',
    headers: {
        'X-CSRFToken': getCookie('csrftoken'),
    }
  })
  .then(() => {
    $('#genderModal').modal('hide')
    submitting.remove()
  })
  .catch(error => {
    console.log(error.response)
  })
}

function hidingMessages(){
  $('#employee-added-message').hide()
  $('#adding-employee-message').hide()
  $('#adding-department-message').hide()
  $('#department-added-message').hide()
  $('#requesting-leave-message').hide()
  $('#leave-requested-message').hide()
  $('#adding-leave-message').hide()
  $('#leave-added-message').hide()
  $('#adding-study-period-message').hide()
  $('#study-period-added-message').hide()
  $('#adding-holiday-message').hide()
  $('#holiday-added-message').hide()
}

// https://stackoverflow.com/questions/3224834/get-difference-between-2-dates-in-javascript
function calculateCalendarDays() {
  const date1 = new Date($('#start-date').val())
  const date2 = new Date($('#end-date').val())
  const diffTime = Math.abs(date2 - date1) // difference is stored in milliseconds
  const diffDays = Math.ceil(diffTime / (1000 * 60 * 60 * 24)) + 1 // milliseconds is converted in days. +1 is added to include current date

  return diffDays
}

function leaveSelect() {
  return  $("#leave-type").val()
}

function diffHours() {
  var valuestart = $("#start-time").val()
  var valuestop = $("#end-time").val()

  /* create date format 
    random date https://stackoverflow.com/questions/11038252/how-can-i-calculate-the-difference-between-two-times-that-are-in-24-hour-format          
  */
  var timeStart = new Date("01/01/2007 " + valuestart).getHours()
  var timeEnd = new Date("01/01/2007 " + valuestop).getHours()
  var hourDiff = timeEnd -timeStart

  if (hourDiff < 0){
    hourDiff = hourDiff + 24
  }
  return hourDiff 
}

function submitLeaveRequest(requestID){
  $('#counter').show()
  const leave_type = $('#leave-type').val()
  var days = $('#actual-days').text()
  var hours = $('#actual-hours').text()
  const start_date = $('#start-date').val()
  var end_date = $('#end-date').val()
  const start_time = $('#start-time').val()
  const end_time = $('#end-time').val()
  const reason = $('#reason').val()
  const payment = $('#leave-pay').val()

  // if the user doesn't press the calculate button
  if (days == "" || hours == "" || days == undefined || hours == undefined){
    $('#calc').click()
    days = parseFloat($('#actual-days').text())
    hours = $('#actual-hours').text()
  }
  else {
    days = parseFloat($('#actual-days').text())

  }
  
  fetch('/leaves')
  .then(response => response.json())
  .then(result => {
    const leaves_list = result
    const requestMessage = $('#requesting-leave-message')
    requestMessage.empty()
    
    // Checking if days_in_lieu_d are not present
    if (leaves_list['days_in_lieu_d'] > 0 && leave_type != 'days_in_lieu_d') {
      requestMessage.empty()
      requestMessage.text("Cannot Request any leave before Days in Lieu are consumed")
      requestMessage.addClass('alert-danger')
      requestMessage.show()
      return
    }

    requestMessage.removeClass('alert-danger')
    requestMessage.removeClass('alert-success')
    
    // Leave Conditions
    if (leave_type == 'short_h' && hours > 3) {
      requestMessage.text("Cannot take short leave of more than 3 hours")
      requestMessage.addClass('alert-danger')
      requestMessage.show()
      return
    }                                         
    else if (leave_type == 'religious_d' && days > leaves_list[leave_type]){
      
      requestMessage.addClass('alert-success')
      requestMessage.text("Days requested are more than alotted days! Additional days will be deducted from Annual Leave")
      requestMessage.show()
    }
    else if (leave_type != 'short_h' && days <= leaves_list[leave_type]){
      requestMessage.addClass('alert-success')
      requestMessage.text("Adding Leave")
      requestMessage.show()
    }
    else if (leave_type == 'short_h' && (hours <= leaves_list[leave_type])) {
      requestMessage.addClass('alert-success')
      requestMessage.text("Adding Leave")
      requestMessage.show()
    }
    else if(leave_type == 'unpaid_d' || leave_type == 'other_d') {
      requestMessage.addClass('alert-success')
      requestMessage.text("Adding Leave")
      requestMessage.show()
    }                                      
    else {
      requestMessage.text("Days requested are more than alotted days!")
      requestMessage.addClass('alert-danger')
      requestMessage.show()
      return
    }
   
    const file_data = $("#supp-doc").prop("files")[0]
    
    
    leave_type == 'short_h' ? days = 0 : hours = 0

    if (leave_type == 'short_h' || leave_type == 'religious_d'){
      end_date = start_date
    }

    // deduct half a day if sick leave start day matches today and there is less than 4 hours left in the day
    const dateChecker = parseDate(start_date).toDateString() == new Date().toDateString()
    if (leave_type == 'sick_d' && dateChecker){
      
      const currentTime = new Date().getHours()
      const endTime = workingHours['end'].getHours()
      const timeDiff = endTime - currentTime
      if (timeDiff < 4 && timeDiff > 0){
        days = days - 0.5
      }
    }
    var data = new FormData()
    data.append("leave-type", leave_type)
    data.append("days", days)   
    data.append("hours", Number(hours))
    data.append("start-date", start_date) 
    data.append("end-date", end_date) 
    data.append("start-time", start_time) 
    data.append("end-time", end_time)
    data.append("reason", reason)
    data.append("leave-pay", payment)

    // checking if user submitted a file
    if(file_data != undefined){
      data.append('files', file_data)
      data.append('name', file_data.name)
    }

    if (requestID == null) {
      fetch('/requests', {
        method: 'post',
        body: data,
        credentials: 'same-origin',
          headers: {
              'X-CSRFToken': getCookie('csrftoken'),
          } 
      })
      .then(() => {
          $('#reqLeaveModal').modal('hide')
          $('#leave-requested-message').show()
          const form = $('#form-request-leave')[0]
          form.reset()
      })
      .catch(error => {
          console.log(error.response)
      })
    }
    else {
      fetch(`/modifyLeaves/${requestID}`, {
        method: 'post',
        body: data,
        credentials: 'same-origin',
          headers: {
              'X-CSRFToken': getCookie('csrftoken'),
          } 
      })
      .then(() => {
          $('#reqLeaveModal').modal('hide')
          $('#leave-requested-message').show()
          const form = $('#form-request-leave')[0]
          form.reset()
      })
      .catch(error => {
          console.log(error.response)
      })
    }
  })
  .catch(error => {
    console.log(error.response)
  })
}

// https://stackoverflow.com/questions/2536379/difference-in-months-between-two-dates-in-javascript
function monthDiff(d1, d2) {
  var months
  months = (d2.getFullYear() - d1.getFullYear()) * 12
  months -= d1.getMonth()
  months += d2.getMonth()
  return months <= 0 ? 0 : months
}

function leavePay(leave_type, leave_pay){

  // resetting the payment before updating it
  leave_pay.val("")

  // paid leaves
  if (leave_type == 'annual_d' || leave_type == 'short_h' || leave_type == 'religious_d' || leave_type == 'bereavement_d' || leave_type == 'days_in_lieu_d') {
    leave_pay.val('Paid')
  }
  // unpaid leaves
  else if (leave_type == 'hajj_d' || leave_type == 'unpaid_d') {
    leave_pay.val('Unpaid')
  }
  // Management approval needed
  else if (leave_type == 'other_d') {
    leave_pay.val('To be decided by the management')
  }
  // conditional based payment
  else if(leave_type == 'sick_d' || leave_type == 'parental_care_d' || leave_type == 'maternity_d') {
    // getting current employee details
    fetch('/employee')
    .then(response => response.json())
    .then(employee => {
        // formatting the date and getting the number of months since joined
        const parts = employee['date'].split('-')
        const mydate = new Date(parts[2], parts[1] - 1, parts[0])
        const difference = monthDiff(mydate, new Date())
        if (leave_type == 'sick_d') {
            difference >= 6 ? leave_pay.val('Paid') : leave_pay.val('Unpaid')            
        }
        else if (leave_type == 'maternity_d' || leave_type == 'parental_care_d'){
          difference >= 12 ? leave_pay.val('Paid') : leave_pay.val('Half-Pay')
        }
    })
    .catch(error => {
      console.log(error.response)
    })
  }
}

function dateHourManager(leave_type){
  const time_container= $('.time-container')
  const end_date_container = $('.end-date-container')
  
  // hiding end date input and showing time input for short leaves
  if (leave_type == 'short_h') {
    time_container.show()
    end_date_container.hide()
    return
  }

  //modifes form to show only start date
  if (leave_type == 'religious_d'){
    time_container.hide()
    end_date_container.hide()
    return
  }
  // hiding time and showing end date if leave type is not short_h
  time_container.hide()
  end_date_container.show()
}

function leaveManager(){
  // setting the minDate of the calender, this will prevent the user from selecting days that have already passed
  minDate()
  var holidaysList
  const container = $('#reqLeave-modal-body')
  const request_leave_form = $('#form-request-leave')
  const loading = $('<div/>')
  .attr({'class': 'alert alert-success', 'role': 'alert'})
  loading.text('Loading...')
  loading.appendTo(container)
  fetch('/employee')
  .then(response => response.json())
  .then(employee => {
    const department = employee.department
    
    /* 
      if the department is one of the mentioned below then show them a warning message for requesting a leave during
      during the study period 
    */
    if (department == "IT Services" || department ==  "Student Services" || department == "Student Success Centre" || department ==  "Foundation School" || department == "Humanities School"|| department == "Graduate School" || department == "Business School" || department == "IT School" || department == "Engineering School"){
      fetch('/department')
      .then(response => response.json())
      .then(department => {
        const startDate = parseDate(department.startDate)
        const endDate = parseDate(department.endDate)
        const currentDate = new Date()
        const request_warning = $('#request-leave-warning').empty().hide()
        if (startDate >= currentDate <= endDate){
          request_warning.attr({'class': 'alert alert-warning', 'role': 'alert'})
          .text("Please write the reason for requesting a leave during the study period.")
          .show()
        } 
      })
    }  
  })
  .then(() => {
    // getting a list of all holidays
    fetch('/holidays')
    .then(response => response.json())
    .then(holidays => {
      holidaysList = holidays
    })
  })
  .then(() => {  
    // whenever the user selects a new leave type
    loading.remove()
    request_leave_form.show()
    $('#leave-type').change(() => {
      $('#days').css({'text-align': 'center'})
      $('#days').hide()
      $('#hours').hide()
      const leave_type = leaveSelect()
      const leave_pay = $('#leave-pay')
      const request_warning = $('#request-leave-warning').empty().hide()
      // checks leave pay 
      leavePay(leave_type, leave_pay)

      // shows/hides date/hour input field depending on the leave type that is selected
      dateHourManager(leave_type)
      
      if (leave_type == "maternity_d"){
          request_warning.attr({'class': 'alert alert-warning', 'role': 'alert'})
          .text('If you want 100 extra maternity leaves, please select the "Other" type and state your reasoning for it.')
          .show()
      }

      /* Calculates leave dates as per leave conditions */
      if (leave_type == 'annual_d') {
        $('#calc').on('click', function() {
          var d1 = $('#start-date').val()
          var d2 = $('#end-date').val()
         var workingDays = workingDaysBetweenDates(d1,d2, holidaysList)
          $('#days').text(`Days Count: ${workingDays}`)
          .css({'text-align': 'center'})
          $('#actual-days').text(workingDays)
          $('#days').show()
        })
      } 
      else if (leave_type == 'sick_d' || leave_type == 'maternity_d' || leave_type == 'hajj_d') {
        $('#calc').on('click', function() {
          var calendarDays =  calculateCalendarDays()
          $('#days').text("Days Count: " + calendarDays)
          $('#actual-days').text(calendarDays)
          $('#days').show()
        })
      }
      else if (leave_type == 'parental_care_d' || leave_type == 'unpaid_d' || leave_type == 'other_d' || leave_type == 'bereavement_d' || leave_type == 'days_in_lieu_d') {
        $('#calc').on('click', function() {
          var d1 = $('#start-date').val()
          var d2 = $('#end-date').val()
          var workingDays = workingDaysBetweenDates(d1,d2, holidaysList)
          $('#days').text(`Days Count: ${workingDays}`)
          .css({'text-align': 'center'})
          $('#actual-days').text(workingDays)
          $('#days').show()
        })
      }
      else if (leave_type == 'short_h') {
        $('#calc').on('click', function() {
          var diffHours = diffHours()
          $('#hours').text("Hours Count: " + diffHours)
          $('#actual-hours').text(diffHours)
          $('#hours').show()
        })
      }
      else if (leave_type == 'religious_d') {
        $('#calc').on('click', function() {
          $('#days').text(`Days Count: 1`)
          $('#actual-days').text("1")
          $('#days').show()
        })
      }
    })
  })
  .catch(error => {
    console.log(error.response)
  })
}

function workingDaysBetweenDates(start, end, holidays){
  var array = []
  var endArray = []
  var i, j, k
  var temp = parseDate(start) //value will be changed later so current value doesn't matter

  // creating a list of all holidays and their ranges
  for (i = 0; i < holidays.length; i++) {
    array.push(parseDate(holidays[i].holidayDate))
    endArray.push(parseDate(holidays[i].holidayEndDate))    
  }
  var startDate = parseDate(start)
  const endDate = parseDate(end) 
  var workingDays = 0
  
  // preventing negative output
  if (endDate < startDate) {
    return 0
  }  

  while(true){
    day = startDate.getDay() //Day of the week

    var holidayCheck = false
    for(j = 0; j < array.length && holidayCheck == false; j++){
      const diffTime = Math.abs(endArray[j] - array[j]) // difference is stored in milliseconds
      const diffDays = Math.ceil(diffTime / (1000 * 60 * 60 * 24)) + 1 // milliseconds is converted in days. +1 is added to include current date

      for(k = 0; k < diffDays && holidayCheck == false; k++) {
        temp = new Date(array[j])
        temp.setDate(array[j].getDate() + k)
        holidayCheck = startDate.getDate() == temp.getDate() &&
          startDate.getMonth() == temp.getMonth() &&
          startDate.getYear() == temp.getYear()
      }
    }
    // if friday or saturday or holiday
    if (day != 5 && day != 6 && !holidayCheck) {        
      workingDays++        
    }    
    
    // stops counting if start and end date matches
    if (startDate.getDate() == endDate.getDate() && startDate.getMonth() == endDate.getMonth() && startDate.getYear() == endDate.getYear()){
      return workingDays
    }
    startDate.setDate(startDate.getDate() + 1)
  }
}

function parseDate(input) {
  // Transform date from text to date
  var parts = input.match(/(\d+)/g)
  // new Date(year, month [, date [, hours[, minutes[, seconds[, ms]]]]])
  return new Date(parts[0], parts[1]-1, parts[2]) // months are 0-based
}

function getCookie(name) {
    let cookieValue = null
    if (document.cookie && document.cookie !== '') {
        const cookies = document.cookie.split(';')
        for (let i = 0; i < cookies.length; i++) {
            const cookie = cookies[i].trim()
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) === (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1))
                break
            }
        }
    }
    return cookieValue
}

function addEmployee(){
  $('#adding-employee-message').empty().html('Adding Employee').show()
  const id = $('#employee-id').val()
  const gender = $('#employee-gender').val()
  const department = $('#employee-department').val()
  const role = $('#employee-role').val()

  fetch('/addEmployee', {
    method: 'post',
    body: JSON.stringify({
        "id": id,
        "gender": gender,
        "department": department,
        "role": role,        
    }),
    credentials: 'same-origin',
    headers: {
        'X-CSRFToken': getCookie('csrftoken'),
    }
  })
  .then(() => {
    $('#employeeModal').modal('hide')
    $('#employee-added-message').show()
    const form = $('#form-add-employee')[0]
    form.reset()
  })
  .catch(error => {
    console.log(error.response)
  })
}

function addEmployeeManager(){
  const department = $('#employee-department').val()
  
  if ($('#employee-role').find(':selected').html() == 'Staff'){
    addEmployee()
  }
  else if ($('#employee-role').find(':selected').html() == 'CEO'){
    fetch(`/CEOChecker`)
    .then(response => response.json())
    .then(result => {
      if (result.length == 0){
        addEmployee()
      }
      else{
        const adding_employee_message = $('#adding-employee-message').empty()
        .html('There can only be one CEO on the system.')
        adding_employee_message.show()
      }
    })
  }
  else {
    fetch(`/departmentHead/${department}`)
    .then(response => response.json())
    .then(result => {
      if (result.length == 0){
        addEmployee()
      }
      else{
        const adding_employee_message = $('#adding-employee-message').empty()
        .html('There can only be one department head per department.')
        adding_employee_message.show()
      }
    })
  }
}

function addDepartment(){
  $('#adding-department-message').show()
  const name = $('#department-name').val()

  // sending post request to add a department
  fetch('/addDepartment', {
      method: 'post',
      body: JSON.stringify({
          "name": name       
      }),
      credentials: 'same-origin',
      headers: {
          'X-CSRFToken': getCookie('csrftoken'),
      }
  })
  .then(() => {
      $('#departmentModal').modal('hide')
      $('#department-added-message').show()
      const form = $('#form-add-department')[0]
      form.reset()
  })
  .catch(error => {
      console.log(error.response)
  })
}

function employeeList(){
  const container = $('#employees-list-container')
  .css({'margin-top': '5px'})
  .empty() // delete child elements if exists
  const loading = $('<div/>')
  .attr({'class': 'alert alert-success', 'role': 'alert'})
  .css({'width': '500px', 'display': 'inline-block'})
  loading.text('Loading...')
  loading.appendTo(container)

  fetch('/employeesList')
  .then(response => response.json())
  .then(employees_list => {   
    const table = $('<table/>')
    .attr({"id": "employees-list-table", "class": "table"})
    const header = $('<h4/>')
    const thead = $('<thead/>')
    const tr = $(
    `<tr>
      <th>ID</th>
      <th>Name</th>
      <th>Department</th>
      <th>Gender</th>
      <th>Join Date</th>
    </tr> `
    )

    const tbody = $('<tbody/>')
    .attr({"id": "employee-list-body"})

    if (employees_list.length > 0){
      loading.remove()
      header.text(`Staff`)
      table.append(thead, tbody)
      thead.append(tr)
      container.append(header, table)
      var i
      for (i = 0; i < employees_list.length; i++){   
        const row = $('<tr/>')
        .attr({'id': `${employees_list[i].employee}-row`})
        const id = $('<td/>')
        .attr({'data-profile': employees_list[i].employee, 'class': 'employee-profile'})
        .text(`${employees_list[i].employee}`)
        .css({'text-decoration': 'underline', 'color': 'blue', 'cursor': 'pointer'})

        const name = $('<td/>').text(`${employees_list[i].firstname} ${employees_list[i].lastname}`)
        const department = $('<td/>').text(`${employees_list[i].department}`)
        const gender = $('<td/>').text(`${employees_list[i].gender}`)
        const joined_date = $('<td/>').text(`${employees_list[i].date}`)

        row.append(id, name, department, gender, joined_date)
        row.appendTo(tbody)

        $('.employee-profile').on("click", function() {
          $('#employees-list-container').empty()
          $('#requests-container').empty()
          const employee = $(this).attr('data-profile')
          $('#employee-leaves-container').empty()
          $('#past-leaves-container').empty()
          remaining_leaves(`leaves/${employee}`, `Leaves`)
          pastLeaves(`pastLeaves/${employee}`, `Past Leaves`)
          pastRequests("Past Requests", `${employee}`)
        })
      }
    }
    else {
      loading.remove()
      const dismiss = $(`
      <div class="alert alert-info alert-dismissible fade show" role="alert">
        No employees found.
        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
      </div>`)
      dismiss.appendTo(container)
    }
  })
  .catch(error => {
    console.log(error.response)
  })
}

function requests(){
  const container = $('#requests-container')
  .css({'margin-top': '5px'})
  .empty() // delete child elements if exists

  // loading container
  const loading = $('<div/>')
  .attr({'class': 'alert alert-success', 'role': 'alert'})
  loading.text('Loading...')
  loading.appendTo(container)

  fetch('/requests')
  .then(response => response.json())
  .then(requests => {
    
    const tbody = $('<tbody/>')
    .attr({"id": "requests-body"})
    if (requests.length > 0){
      loading.remove()
      const table = $('<table/>')
      .attr({"id": "requests-table", "class": "table"})
      const header = $('<h4/>').text('Leave Requests')
      const thead = $('<thead/>')
      const tr = $(
      `<tr>
        <th>ID</th>
        <th>Name</th>
        <th>Leave Type</th>
        <th>Details</th>
        <th>Approve/Reject</th>
      </tr> `
      )
      var i
      table.append(thead, tbody)
      thead.append(tr)
      container.append(header, table)
      
      for (i = 0; i < requests.length; i++){
        const row = $('<tr/>')
        .attr({'id': `${requests[i].requestID}-request-row`})
        const id = $('<td/>').text(`${requests[i].employee}`)
        const name = $('<td/>').text(`${requests[i].name}`)
        const leave_type = $('<td/>').text(leaveTypes(requests[i].leave))
        let reason_button_cell = $('<td/>')
        let reason = $('<button/>')
        .attr({
          'type': 'button', 'class': 'btn btn-secondary view-leave',
          "data-request": `${requests[i].requestID}`, "data-bs-toggle": "modal", 
          "data-bs-target": "#viewModal"
        })
        .text("View")
        .css({'vertical-align': 'middle'})

        let decision_button_cell = $('<td/>')
        let approve_button = $('<button/>')
        .attr({
          'type': 'button', 'class': 'btn btn-success decision', 
           'data-decision': `${requests[i].requestID},Approved`
        })
        .text('Approve')
        .css({'margin': '5px'})
        let reject_button = $('<button/>')
        .attr({
          'type': 'button', 'class': 'btn btn-danger decision', 
          'data-decision': `${requests[i].requestID},Rejected`
        })
        .text('Reject')

        reason.appendTo(reason_button_cell)
        decision_button_cell.append(approve_button, reject_button)
        row.append(id, name, leave_type, reason_button_cell, decision_button_cell)
        row.appendTo(tbody)
      }
      $('.view-leave').on("click", function() {
        viewDetails($(this).attr('data-request'))
      
      })

      $('.decision').on("click", function () {
          requestsHandler($(this).attr('data-decision'))
      })
    }
    else {
      loading.remove()
      const dismiss = $(`
      <div class="alert alert-info alert-dismissible fade show" role="alert">
        No pending requests
        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
      </div>`)

    dismiss.appendTo(container)
    }
  })
  .catch(error => {
    console.log(error.response)
  })
}

// returns user friendly leave type name
function leaveTypes(leave_type){
  if (leave_type == 'annual_d')
    return "Annual"
  else if (leave_type == 'sick_d')
    return "Sick"
    else if (leave_type == 'parental_care_d')
    return "Parental Care"
  else if (leave_type == 'maternity_d')
    return "Maternity"   
  else if (leave_type == 'hajj_d')
    return "Hajj"
  else if (leave_type == 'short_h')
    return "Short"
  else if (leave_type == 'religious_d')
    return "Religious Half-Day"
  else if (leave_type == 'bereavement_d')
    return "Bereavement"
  else if (leave_type == 'unpaid_d')
    return "Unpaid"
  else if (leave_type == 'days_in_lieu_d')
    return "Days in Lieu"
  else if (leave_type == 'other_d')
    return "Other"
}

function viewDetails(req, bCheck=true){

  /* 
    if view button is not clicked then delete all the previous instances and the event triggers of approve/reject
    buttons inside the view button.
  */
  if(bCheck == false){
    $('.viewDecision').remove()
  }

  const view_leave_body = $('#view-leave-body').empty()
  const loading = $('<div/>')
  .attr({'class': 'alert alert-success', 'role': 'alert'})
  loading.text('Loading...')
  loading.appendTo(view_leave_body)
  fetch(`/requests/${req}`)
  .then(response => response.json())
  .then(request => {
    loading.remove()
    // Adding all details when view button is clicked
    const form = $('<form/>')
    .attr({'id':'request-decision-form'})

    const form_body = (`<div>
      <div class="mb-3">
        <label class="left ">ID</label>
        <input disabled type="text" class="form-control" value=${request.employee}>
      </div>
      <div class="mb-3">
        <label class="left">Name</label>
        <input disabled type="text" class="form-control" value='${request.name}'>
      </div>
      <div class="mb-3">
        <label class="left">Leave Type</label>
        <input disabled type="text" class="form-control" value=${leaveTypes(request.leave)}>
      </div>
      <div class="mb-3">
        <label class="left">Days</label>
        <input disabled type="text" class="form-control" value=${request.days}>
      </div>
      <div class="mb-3">
        <label class="left">Hours</label>
        <input disabled type="text" class="form-control" value=${request.hours}>
      </div>
      <div class="mb-3">
        <label class="left">Start Date</label>
        <input disabled type="text" class="form-control" value=${request.startDate}>
      </div>
      <div class="mb-3">
        <label class="left">End Date</label>
        <input disabled type="text" class="form-control" value=${request.endDate}>
      </div>
      <div class="mb-3 form-group">
        <label class="left">Start Time</label>
        <input disabled type="text" class="form-control" value=${request.startTime}>
      </div>
      <div class="mb-3">
        <label class="left">End Time</label>
        <input disabled type="text" class="form-control" value=${request.endTime}>
      </div>
      <div class="mb-3">
        <label class="left">Payment</label>
        <select class="form-control" required id='payment-dropdown' class='form-control'>
          <option value="${request.payment}">${request.payment} </option>
        </select> 
      </div>
      <div class="mb-3">
        <label class="left">Reason</label>
        <textarea disabled type="text" class="form-control">${request.reason}</textarea>
      </div>
      <div class="mb-3">
        <a href="/suppDoc/${request.document}" rel='noopener noreferrer' target='_blank'> ${request.document} </a>
      </div>
      <div class="mb-3">
        <label class="left">Comment</label>
        <textarea id="request-comment" type="text" class="form-control">${request.comment}</textarea>
      </div>
      <div class="mb-3">
        <label class="left">Decision</label>
        <input disabled type="text" class="form-control" value=${request.decision}>
      </div>
    </div> `)

    form.append(form_body)
    view_leave_body.append(form)

    if (bCheck){
      const form_footer = $('#view-leave-footer').empty()
      let approve_button = $('<button/>')
      .attr({
        'type': 'button', 'class': 'btn btn-success viewDecision', 
         'data-viewDecision': `${req},Approved`
      })
      .text('Approve')
      .css({'margin': '5px'})
      let reject_button = $('<button/>')
      .attr({
        'type': 'button', 'class': 'btn btn-danger viewDecision', 
        'data-viewDecision': `${req},Rejected`
      })
      .text('Reject')
      form_footer.append(approve_button, reject_button)
    }
  })
  .then(() => {    
    if (bCheck){
      const payment_dropdown = $('#payment-dropdown')
      const values = ["Paid", "Unpaid", "Half-Pay"]
      var i
      for (i = 0; i < values.length; i++){
        const option = $('<option/>')
        if (values[i] != payment_dropdown.val()){
          option.attr({'value': values[i]}).text(values[i])
          .appendTo(payment_dropdown)
        }
      }
    }
  })
  .then(() => {
    $('.viewDecision').on("click", function () {
      $('#viewModal').modal('hide')
      requestsHandler($(this).attr('data-viewDecision'))
    })
  })
  .catch(error => {
    console.log(error.response)
  })
}
function remaining_leaves(url, title){
  const container = $('#employee-leaves-container').empty()
  const loading = $('<div/>')
  .attr({'class': 'alert alert-success', 'role': 'alert'})
  .css({'width': '500px', 'display': 'inline-block'})
  loading.text('Loading...')
  loading.appendTo(container)

  fetch(url)
  .then(response => response.json())
  .then(employee => {
    loading.remove()
    const table = $('<table/>')
    .attr({"id": "employee-leaves-table", "class": "table"})
    .css({'width': '50%', 'margin': '0 auto'}) 
    const header = $('<h4/>').text(`${title}`)
    const tbody = $('<tbody/>')
    .attr({"id": "requests-body"})
    const tr = $(
      `<tr>
        <th>Name</th>
        <td>${employee.name}
      </tr>
      <tr>
        <th>Annual</th>
        <td>${employee.annual_d} day(s)
      </tr>
      <tr>
        <th>Sick</th>
        <td>${employee.sick_d} day(s)
      </tr>
      <tr>
        <th>Maternity</th>
        <td>${employee.maternity_d} day(s)
      </tr>
      <tr>
      <th>Parental Care</th>
      <td>${employee.parental_care_d} day(s)
      </tr>
      <tr>
      <th>Bereavement</th>
      <td>${employee.bereavement_d} day(s)
      </tr>
      <tr>
      <th>Days in Lieu</th>
      <td>${employee.days_in_lieu_d} day(s)
      </tr>
      <tr>
      <th>Hajj</th>
      <td>${employee.hajj_d} day(s)
      </tr>
      <tr>
        <th>Religious</th>
        <td>${employee.religious_d} day(s)
        </tr>       
        <tr>
          <th>Short</th>
          <td>${employee.short_h} hour(s)
          </tr>
          <tr>`
      )
      var i
      table.append(tbody)
      tbody.append(tr)
      container.append(header, table)
  })
  .catch(error => {
    console.log(error.response)
  })
}

function requestsHandler(data){
  const dataArr = String(data).split(',')
  const requestID = dataArr[0] // getting the request id of the button that was just clicked
  const decision = dataArr[1] // getting the decision value; approve or reject
  const payment = $('#payment-dropdown').val()
  const comment = $('#request-comment').val()
  const request_row = $(`#${requestID}-request-row`)
  .animate({"opacity": "0"}, () => {
    request_row.remove()
  })
  
  fetch(`/requests/${requestID}`, {
    method: 'PUT',
    body: JSON.stringify({
      "decision": decision,
      "payment": payment,
      "comment": comment
    }),
    credentials: 'same-origin',
    headers: {
        'X-CSRFToken': getCookie('csrftoken'),
    }
  })
  .then(() => {
    fetch('/requests')
    .then(response => response.json())
    .then(result => {
      if (result.length == 0)
      {
          const container = $('#requests-container').empty()
          const dismiss = $(`
          <div class="alert alert-info alert-dismissible fade show" role="alert">
            No pending requests
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
          </div>`)

          dismiss.appendTo(container)
      }
    })
    .catch(error => {
      console.log(error.response)
    })
  })
  .catch(error => {
    console.log(error.response)
  })
}

function pastLeaves(url, title){
  const container = $('#past-leaves-container').empty()
  const loading = $('<div/>')
  .attr({'class': 'alert alert-success', 'role': 'alert'})
  .css({'width': '500px', 'display': 'inline-block'})
  loading.text('Loading...')
  loading.appendTo(container) 

  fetch(url)
  .then(response => response.json())
  .then(pastLeaves => {

    if(pastLeaves.length > 0){
      loading.remove()
      const table = $('<table/>')
      .attr({"id": "past-leaves-table", "class": "table"})
      .css({'margin': '0 auto'})
      const header = $('<h4/>').text(`${title}`)
      const thead = $('<thead/>')
      const tr = $(
        `<tr>
          <th>ID</th>
          <th>Name</th>
          <th>Leave Type</th>
          <th>Details</th>
        </tr> `
        )
      container.append(header)
      thead.append(tr)
      table.append(thead)  
      container.append(table)
      const tbody = $('<tbody/>') 

      for (i = 0; i < pastLeaves.length; i++){
        const row = $('<tr/>')
        .attr({'id': `${pastLeaves[i].requestID}-past-leaves-row`})
        const id = $('<td/>').text(`${pastLeaves[i].employee}`)
        const name = $('<td/>').text(`${pastLeaves[i].name}`)
        const leave_type = $('<td/>').text(leaveTypes(pastLeaves[i].leave))
        let view_button_cell = $('<td/>')
        let view = $('<button/>')
        .attr({
          'type': 'button', 'class': 'btn btn-secondary past-leave',
          "data-viewPastLeaves": `${pastLeaves[i].requestID}`, "data-bs-toggle": "modal", 
          "data-bs-target": "#viewModal"
        })
        .text("View")
        .css({'vertical-align': 'middle'})

        view.appendTo(view_button_cell)
        row.append(id, name, leave_type, view_button_cell)
        row.appendTo(tbody)
        table.append(tbody)
      } 
      $('.past-leave').on("click", function() {
        viewDetails($(this).attr('data-viewPastLeaves'), false)
      })
    }
    else {
      loading.remove()
      const dismiss = $(`
      <div class="alert alert-info alert-dismissible fade show" role="alert">
        No past leaves.
        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
      </div>`)

      dismiss.appendTo(container)
    }
  })
  .catch(error => {
    console.log(error.response)
  })   
}

function pastRequests(title, url=""){
  var myRequests, pastRequests
  const container = $('#past-requests-container').empty()
  const loading = $('<div/>')
  .attr({'class': 'alert alert-success', 'role': 'alert'})
  .css({'width': '500px', 'display': 'inline-block'})
  loading.text('Loading...')
  loading.appendTo(container) 
  
  fetch('/pastRequests')
  .then(response => response.json())
  .then(results => {
      myRequests = results
  })
  .then(() => {
    fetch(`/pastRequests/${url}`)
    .then(response => response.json())
    .then(pastReq => {
      pastRequests = pastReq
      
      if(pastRequests.length > 0){
        loading.remove()
        const table = $('<table/>')
        .attr({"id": "past-requests-table", "class": "table"})
        .css({'margin': '0 auto'})
        const header = $('<h4/>').text(`${title}`)
        const thead = $('<thead/>')
        const tr = $(
          `<tr>
            <th>Name</th>
            <th>Leave</th>
            <th>Start Date</th>
            <th>End Date</th>
            <th>Options</th>
          </tr> `
          )
        container.append(header)
        thead.append(tr)
        table.append(thead)  
        container.append(table)
        const tbody = $('<tbody/>') 

        for (i = 0; i < pastRequests.length; i++){
          const row = $('<tr/>')
          .attr({'id': `${pastRequests[i].requestID}-past-requests-row`})
          const name = $('<td/>').text(`${pastRequests[i].name}`)
          const leave_type = $('<td/>').text(leaveTypes(pastRequests[i].leave))
          const start_date = $('<td/>').text(`${pastRequests[i].startDate}`)
          const end_date = $('<td/>').text(`${pastRequests[i].endDate}`)
          let option_buttons_cell = $('<td/>')
          let view = $('<button/>')
          .attr({
            'type': 'button', 'class': 'btn btn-secondary past-request',
            "data-viewPastRequests": `${pastRequests[i].requestID}`, "data-bs-toggle": "modal", 
            "data-bs-target": "#viewModal"
          })
          .text("View")
          .css({'vertical-align': 'middle'})
          let cancel = $('<button/>')
          .attr({
            'type': 'button', 'class': 'btn btn-danger cancel-request',
            "data-cancelPastRequests": `${pastRequests[i].requestID}`
          })
          .text("Cancel")
          .css({'margin': '5px'})

          option_buttons_cell.append(view)
          if (pastRequests[i].decision == "Approved" && parseDate(myRequests[i].startDate) > new Date()){
            option_buttons_cell.append(cancel)
          }
          row.append(name, leave_type, start_date, end_date, option_buttons_cell)
          row.appendTo(tbody)
          table.append(tbody)
        } 
        $('.past-request').on("click", function() {
          viewDetails($(this).attr('data-viewPastRequests'), false)
        })

        $('.cancel-request').on("click", function() {
          deleteApprovedRequest($(this).attr('data-cancelPastRequests'))
        })
      }
      else {
        loading.remove()
        const dismiss = $(`
        <div class="alert alert-info alert-dismissible fade show" role="alert">
          No past requests.
          <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
        </div>`)

        dismiss.appendTo(container)
      }
    })  
  })
}

function deleteRequest(requestID) {
  const request_row = $(`#${requestID}-pending-requests-row`)
  .animate({"opacity": "0"}, () => {
    request_row.remove()
  })
  
  fetch(`/deleteRequests/${requestID}`, {
    method: 'DELETE',
    body: null,
    credentials: 'same-origin',
    headers: {
        'X-CSRFToken': getCookie('csrftoken'),
        'Content-Type': 'application/json'
    }
  })
  .then(() => {
    fetch('/pendingRequests')
    .then(response => response.json())
    .then(result => {
      if (result.length == 0)
      {
          const container = $('#pending-requests-container').empty()
          const dismiss = $(`
          <div class="alert alert-info alert-dismissible fade show" role="alert">
            No pending requests
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
          </div>`)

          dismiss.appendTo(container)
      }
    })
    .catch(error => {
      console.log(error.response)
    })
  })
  .catch(error => {
    console.log(error.response)
  })
}

function deleteApprovedRequest(requestID) {
  const request_row = $(`#${requestID}-past-requests-row`)
  .animate({"opacity": "0"}, () => {
    request_row.remove()
  })
  
  fetch(`/deleteRequests/${requestID}`, {
    method: 'DELETE',
    body: null,
    credentials: 'same-origin',
    headers: {
        'X-CSRFToken': getCookie('csrftoken'),
        'Content-Type': 'application/json'
    }
  })
  .then(() => {
    fetch('/pastRequests')
    .then(response => response.json())
    .then(result => {
      if (result.length == 0)
      {
          const container = $('#past-requests-container').empty()
          const dismiss = $(`
          <div class="alert alert-info alert-dismissible fade show" role="alert">
            No past requests
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
          </div>`)

          dismiss.appendTo(container)
      }
    })
    .catch(error => {
      console.log(error.response)
    })
  })
  .catch(error => {
    console.log(error.response)
  })
}

function modifyRequests(requestID) {
  leaveManager()
  globalRequestID = requestID
}

function pendingRequests(title, url=""){
  const container = $('#pending-requests-container').empty()
  const loading = $('<div/>')
  .attr({'class': 'alert alert-success', 'role': 'alert'})
  .css({'width': '500px', 'display': 'inline-block'})
  loading.text('Loading...')
  loading.appendTo(container) 
  
  fetch(`/pendingRequests/${url}`)
  .then(response => response.json())
  .then(pendingRequests => {
    if(pendingRequests.length > 0){
      loading.remove()
      const table = $('<table/>')
      .attr({"id": "pending-requests-table", "class": "table"})
      .css({'margin': '0 auto'})
      const header = $('<h4/>').text(`${title}`)
      const thead = $('<thead/>')
      const tr = $(
        `<tr>
          <th>Name</th>
          <th>Leave</th>
          <th>Start Date</th>
          <th>End Date</th>
          <th>Details</th>
          <th>Options</th>
        </tr> `
        )
      container.append(header)
      thead.append(tr)
      table.append(thead)  
      container.append(table)
      const tbody = $('<tbody/>') 

      for (i = 0; i < pendingRequests.length; i++){
        const row = $('<tr/>')
        .attr({'id': `${pendingRequests[i].requestID}-pending-requests-row`})
        const name = $('<td/>').text(`${pendingRequests[i].name}`)
        const leave_type = $('<td/>').text(leaveTypes(pendingRequests[i].leave))
        const start_date = $('<td/>').text(`${pendingRequests[i].startDate}`)
        const end_date = $('<td/>').text(`${pendingRequests[i].endDate}`)
        let view_button_cell = $('<td/>')
        let view = $('<button/>')
        .attr({
          'type': 'button', 'class': 'btn btn-secondary view-request',
          "data-viewPendingRequests": `${pendingRequests[i].requestID}`, "data-bs-toggle": "modal", 
          "data-bs-target": "#viewModal"
        })
        .text("View")
        .css({'vertical-align': 'middle'})

        let option_buttons_cell = $('<td/>')
        let del = $('<button/>')
        .attr({
          'type': 'button', 'class': 'btn btn-danger delete-request',
          "data-deletePendingRequests": `${pendingRequests[i].requestID}`
        })
        .text("Delete")
        .css({'margin': '5px'})

        let mod = $('<button/>')
        .attr({
          'type': 'button', 'class': 'btn btn-dark modify-request',
          "data-modifyPendingRequests": `${pendingRequests[i].requestID}`, "data-bs-toggle": "modal", 
          "data-bs-target": "#reqLeaveModal"
        })
        .text("Modify")

        option_buttons_cell.append(mod, del)
        view.appendTo(view_button_cell)
        row.append(name, leave_type, start_date, end_date, view_button_cell, option_buttons_cell)
        row.appendTo(tbody)
        table.append(tbody)
      } 
      $('.view-request').on("click", function() {
        viewDetails($(this).attr('data-viewPendingRequests'), false)
      })

      $('.delete-request').on("click", function() {
        deleteRequest($(this).attr('data-deletePendingRequests'))
      })

      $('.modify-request').on("click", function() {
        modifyRequests($(this).attr('data-modifyPendingRequests'))
      })
    }
    else {
      loading.remove()
      const dismiss = $(`
      <div class="alert alert-info alert-dismissible fade show" role="alert">
        No pending requests.
        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
      </div>`)

      dismiss.appendTo(container)
    }
  }) 
  .catch(error => {
    console.log(error.response)
  })  
}

function addStudyPeriod(){
  $('#adding-study-period-message').show()
  const start_date = $('#study-start-date').val()
  const end_date = $('#study-end-date').val()
  const department = $('#study-period-department').val()

  fetch('/addStudyPeriod', {
    method: 'put',
    body: JSON.stringify({
        "start_date": start_date,
        "end_date": end_date,
        "department": department,      
    }),
    credentials: 'same-origin',
    headers: {
        'X-CSRFToken': getCookie('csrftoken'),
    }
  })
  .then(() => {
    $('#studyPeriodModal').modal('hide')
    $('#study-period-added-message').show()
    const form = $('#form-study-period')[0]
    form.reset()
  })
  .catch(error => {
    console.log(error.response)
  })
}

function delegateEmployeeHandler(){
  $('#adding-delegate-message').hide()
  const loading_delegating_body = $('#loading-delegate-employee')
  const loading = $('<div/>')
  .attr({'class': 'alert alert-success', 'role': 'alert'})
  loading.text('Loading...')
  loading.appendTo(loading_delegating_body)

  const employee_dropdown = $('#employee-dropdown')
    
  fetch('/employeesList')
  .then(response => response.json())
  .then(employees => {
    var i
    for (i = 0; i < employees.length; i++){
      const select = $('<option/>')
      .val(employees[i].employee)
      .text(`${employees[i].firstname} ${employees[i].lastname}`)
      select.appendTo(employee_dropdown)
    }
  })
  .then(() => {
    loading.remove()
    $('#submit-delegate-employee').click((event) => {
      event.preventDefault()
      $('#adding-delegate-message').show()
      delegateSubmitHandler()
    })
  })
  .catch(error => {
    console.log(error.response)
  })
}

function delegateSubmitHandler(){
  const employee = $('#employee-dropdown').val()
  const start_date = $('#delegate-start-date').val()
  const end_date = $('#delegate-end-date').val()

  fetch('/delegate', {
    method: 'post',
        body: JSON.stringify({
          "employee": employee,
          "start-date": start_date,
          "end-date": end_date
        }),
        credentials: 'same-origin',
          headers: {
              'X-CSRFToken': getCookie('csrftoken'),
          } 
  })
  .then(() => {
    $('#delegateModal').modal('hide')
  })
  .catch(error => {
    console.log(error.response)
  })
}

function carryForwardLeavesHandler(){
  $('#saving-carry-forward-leaves-message').hide()

  $('#submit-carry-forward-leaves').click((event) => {
    event.preventDefault()
    carryForwardSubmitHandler()
  })
}

function carryForwardSubmitHandler(){
  $('#saving-carry-forward-leaves-message').show()

  const employee = $('#carry-forward-employee').val()
  var carry_forward = $("input[name='carry-forward-permission']:checked").val()
  carry_forward == "Yes" ? carry_forward = true : carry_forward = false
  
  fetch('/carryForwardLeaves', {
    method: 'PUT',
    body: JSON.stringify({
      "employee": employee,
      "carry-forward": carry_forward
    }),
    credentials: 'same-origin',
    headers: {
        'X-CSRFToken': getCookie('csrftoken'),
    }
  })
  .then(() => {
    $('#saving-carry-forward-leaves-message').hide()
    $('#carryForwardLeavesModal').modal('hide')
  })
}

function addLeaveManager() {
  $('#add-leave-type').change(() => {
    if ($('#add-leave-type').val() == 'short_h') {
      $('.days-container').hide()
      $('.hours-container').show()
    }
    else {
      $('.days-container').show()
      $('.hours-container').hide()
    }
  })
}

function addLeave(){ 
  $('#adding-leave-message').show()
  const employee = $('#employee-leave-id').val()
  const leave_type = $('#add-leave-type').val()
  const days = $('#leave-days').val()
  const hours = $('#leave-hours').val()
  const reason = $('#reason-add-leave').val()

  // updates the amount of leaves
  fetch(`/leaves/${employee}`, {
      method: 'PUT',
      body: JSON.stringify({
        "add-leave-type": leave_type,
        "leave-days": days,
        "leave-hours": hours,
        "reason": reason,
      }),
      credentials: 'same-origin',
      headers: {
          'X-CSRFToken': getCookie('csrftoken'),
      }
  })
  .then(() => {
      $('#addLeaveModal').modal('hide')
      $('#leave-added-message').show()
      const form = $('#form-add-leave')[0]
      form.reset()
  })
  .catch(error => {
      console.log(error.response)
  })
}

function removeLeave(){  
  $('#adding-leave-message').show()
  const employee = $('#employee-leave-id').val()
  const leave_type = $('#add-leave-type').val()
  var days = $('#leave-days').val()
  var hours = $('#leave-hours').val()
  const reason = $('#reason-add-leave').val()

  days = -(days)
  hours = -(hours)
  // updates the amount of leaves
  fetch(`/leaves/${employee}`, {
      method: 'PUT',
      body: JSON.stringify({
        "add-leave-type": leave_type,
        "leave-days": days,
        "leave-hours": hours,
        "reason": reason,
      }),
      credentials: 'same-origin',
      headers: {
          'X-CSRFToken': getCookie('csrftoken'),
      }
  })
  .then(() => {
      $('#addLeaveModal').modal('hide')
      $('#leave-added-message').show()
      const form = $('#form-add-leave')[0]
      form.reset()
  })
  .catch(error => {
      console.log(error.response)
  })
}

// sets the mininum date in the calender in the request leave form
function minDate() {
  var dtToday = new Date()
  var month = dtToday.getMonth() + 1
  var day = dtToday.getDate()
  var year = dtToday.getFullYear()
  
  if(month < 10)
      month = '0' + month.toString()
  if(day < 10)
      day = '0' + day.toString()
  
  var maxDate = year + '-' + month + '-' + day

  $('.set-min').attr('min', maxDate)
}

function addHoliday() {
  $('#adding-holiday-message').show()
  const holidayName = $('#holiday-name').val()
  const holidayDate = $('#holiday-date').val()
  const holidayEndDate = $('#holiday-end-date').val()

  // updates the amount of leaves
  fetch(`/holidays`, {
      method: 'POST',
      body: JSON.stringify({
        "holiday-name": holidayName,
        "holiday-date": holidayDate,
        "holiday-end-date": holidayEndDate
      }),
      credentials: 'same-origin',
      headers: {
          'X-CSRFToken': getCookie('csrftoken'),
      }
  })
  .then(() => {
      $('#addHolidayModal').modal('hide')
      $('#holiday-added-message').show()
      const form = $('#form-add-leave')[0]
      form.reset()
  })
  .catch(error => {
      console.log(error.response)
  })
}

function remove_holiday(holiday){

  const holiday_row = $(`#${holiday}-holidays-row`)
  .animate({"opacity": "0"}, () => {
    holiday_row.remove()
  })
  // updates the amount of leaves
  fetch(`/removeholiday`, {
      method: 'POST',
      body: JSON.stringify({
        "id": holiday,
      }),
      credentials: 'same-origin',
      headers: {
          'X-CSRFToken': getCookie('csrftoken'),
      }
  })
  .then(() => {
    fetch('/holidays')
    .then(response => response.json())
    .then(result => {
      if (result.length == 0)
      {
          const container = $('#holidays-container').empty()
          const dismiss = $(`
          <div class="alert alert-info alert-dismissible fade show" role="alert">
            No holidays.
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
          </div>`)

          dismiss.appendTo(container)
      }
    })
    .catch(error => {
      console.log(error.reponse)
    })
  })
}

function view_holidays(){
  const container = $('#holidays-container').empty()
  const loading = $('<div/>')
  .attr({'class': 'alert alert-success', 'role': 'alert'})
  .css({'width': '500px', 'display': 'inline-block'})
  loading.text('Loading...')
  loading.appendTo(container) 
  
  fetch(`/holidays`)
  .then(response => response.json())
  .then(holidays => {
    if(holidays.length > 0){
      loading.remove()
      const table = $('<table/>')
      .attr({"id": "holidays-table", "class": "table"})
      .css({'margin': '0 auto'})
      const header = $('<h4/>').text(`Holidays`)
      const thead = $('<thead/>')
      const tr = $(
        `<tr>
          <th>Holiday Name</th>
          <th>Start Date</th>
          <th>End Date</th>
        </tr> `
        )
      container.append(header)
      thead.append(tr)
      table.append(thead)  
      container.append(table)
      const tbody = $('<tbody/>') 
        if ($.trim($('#current-department').html()) === 'People and Culture' || 
            $.trim($('#current-role').html()) === 'CEO'){

          tr.append('<th>Options</th>')
        }

      for (i = 0; i < holidays.length; i++){
        const row = $('<tr/>')
        .attr({'id': `${holidays[i].holidayID}-holidays-row`})
        const name = $('<td/>').text(`${holidays[i].holidayName}`)
        const start_date = $('<td/>').text(`${holidays[i].holidayDate}`)
        const end_date = $('<td/>').text(`${holidays[i].holidayEndDate}`)
        

        if ($.trim($('#current-department').html()) === 'People and Culture' || 
            $.trim($('#current-role').html()) === 'CEO'){

          let option_buttons_cell = $('<td/>')
          let delete_button = $('<button/>')
          .attr({
          'type': 'button', 'class': 'btn btn-danger delete-holiday',
          "data-Holidays": `${holidays[i].holidayID}`
          })
          .text("Delete")
          .css({'margin': '5px'})

          delete_button.appendTo(option_buttons_cell)
          row.append(name, start_date, end_date, option_buttons_cell)
        }
        else {
          row.append(name, start_date, end_date)
        }
        row.appendTo(tbody)
        table.append(tbody)
      }
      if ($.trim($('#current-department').html()) === 'People and Culture' ||
        $.trim($('#current-role').html()) === 'CEO'){
        $('.delete-holiday').on("click", function() {
          remove_holiday($(this).attr('data-holidays'))
        }) 
      }
    }
    else {
      loading.remove()
      const dismiss = $(`
      <div class="alert alert-info alert-dismissible fade show" role="alert">
        No holidays.
        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
      </div>`)

      dismiss.appendTo(container)
    }
  }) 
  .catch(error => {
    console.log(error.response)
  })  
}