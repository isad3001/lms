from celery import shared_task
from .models import *
import datetime
from django.db.models import Q

@shared_task
def reset_annual_leaves():
    employees = Employee.objects.exclude(Q(department__name= "Outreach and Admission") | Q(department__name="Marketing, Business Development and Sales"))    
    
    for employee in employees:
        leave = Leave.objects.get(employee=employee)

        if not leave.annual_carry_forward: 
            leave.annual_d=22
        leave.sick_d=15  
        leave.parental_care_d=5 
        leave.short_h=36 
        leave.religious_d=1

        if employee.gender == 'F':
            leave.maternity_d=45
        leave.save()

    return True
    
@shared_task
def reset_annual_leaves_special():
    employees = Employee.objects.filter(Q(department__name= "Outreach and Admission") | Q(department__name="Marketing, Business Development and Sales"))    
    for employee in employees:
        leave = Leave.objects.get(employee=employee)
        leave.annual_d=22
        leave.sick_d=15 
        leave.maternity_d=45 
        leave.parental_care_d=5 
        leave.short_h=36 
        leave.religious_d=1
        leave.save()

    return True

@shared_task
def reset_days_in_lieu():
    mods = Modification.objects.filter(leave='days_in_lieu_d')
    
    for mod in mods:
        if (datetime.date.today() - mod.lieu_counter).days > 28 and mod.days > 0 and mod.lieu_check == "untouched":
            leave = Leave.objects.get(employee=mod.employee)
            leave.days_in_lieu_d = leave.days_in_lieu_d - int(mod.days)
            
            if leave.days_in_lieu_d < 0:
                leave.days_in_lieu_d = 0

            leave.save()
            mod.lieu_check = "touched"
            mod.save()
    return True