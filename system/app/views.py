from django.shortcuts import render
from django.contrib.auth import authenticate, login, logout
from django_auth_ldap.backend import LDAPBackend
from datetime import datetime
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse, Http404
from django.urls import reverse
from django.contrib.auth.decorators import login_required
from .models import *
import json
from django.db.models import Q
from .miscellaneous import *
from decimal import *

def login_view(request):
    # prevent access to login page if the user is already authenticated 
    if request.user.is_authenticated:
        return HttpResponseRedirect(reverse('index'))
    if request.method == "POST":
        username = request.POST.get("username")
        password = request.POST.get("password")

        # LDAP authentication
        auth = LDAPBackend()
        user = auth.authenticate(request, username=username, password=password)

        if user is not None:
            # backend argument needed when more than 2 backends added in settings
            login(request, user, backend="django_auth_ldap.backend.LDAPBackend")
            
            # formatting the data
            date = datetime.strptime(user.date, '%Y%m%d%H%M%S.%fZ').date()

            user.date = date.strftime('%d-%m-%Y')
            user.is_active = True
            if user.is_superuser:
                user.is_staff = True
                employee = Employee.objects.get(username=username)
                leave = Leave.objects.filter(employee=employee)
                user.role = Role.objects.get(name='Head of Department')
                user.department = Department.objects.get(name='People and Culture')

                if len(leave) == 0: 
                    # if employee joined less than 6 months ago then their annual leaves is set to 11 as supposed to 22
                    if dateDiff(date):
                        Leave(employee=employee, annual_d=11).save()
                    else:    
                        Leave(employee=employee).save()
            
            user.save()
            
            delegate = Delegate.objects.filter(employee=user)
            
            if len(delegate) != 0:
                delegate = delegate[0]
            else:
                delegate = Delegate(employee=user)
            
            delegate.save()
            user.save()

            # checking if the user is authorized to access the application            
            if not user.is_staff:
                delegate.delete()
                user.delete()
                logout(request)
                return render(request, "app/login.html", {
                    "error": "You are not authorized to access this page."
                })
            else:
                return HttpResponseRedirect(reverse("index"))
        else:
            return render(request, "app/login.html", {
                    "error": "Invalid credentials entered! Please try again, make sure caps is not on."
                })

    return render(request, "app/login.html")

def logout_view(request):
    logout(request)
    return HttpResponseRedirect(reverse('login'))

@login_required(login_url='/login')
def index(request):
    user = Employee.objects.get(id=request.user.id)
    delegate = Delegate.objects.get(employee=user)
    context = {
        "user": request.user,
        "departments": Department.objects.all(),
        "roles": Role.objects.all(),
        "genders": Employee.GENDER,
        "leaves": Request.LEAVES,
    }

    if delegate.startDate is not None or delegate.endDate is not None:
        if timeDiff(delegate.startDate, delegate.endDate):
            delegate.status = "Active"
            
        else:
            delegate.status = 'Inactive'
        
        delegate.save()      
    context["delegate"] = delegate.status

    department = user.department
    if user.role.name == "CEO":
        employee = Employee.objects.all().exclude(username=user.username)
        context['employees'] = employee
    elif user.department.name == "People and Culture" and user.role.name == "Head of Department":
        employee = Employee.objects.all().exclude(username=user.username)
        context['employees'] = employee
    elif user.department.name == "People and Culture":
        role = Role.objects.get(name='Head of Department')
        head = Employee.objects.get(department=department, role=role)
        employee = Employee.objects.all().exclude(Q(username=user.username) & Q(username=head.username) & Q(role=Role.objects.get(name='CEO')))
        context['employees'] = employee
    elif user.role.name == "Head of Department":
        employee = Employee.objects.filter(department=department).exclude(username=user.username)
        context['employees'] = employee
         
    return render(request, "app/index.html", context)

@login_required
def add_employee(request):
    if request.method == "POST":
        body = json.loads(request.body)
        user_id = body.get('id', "")
        gender = body.get('gender', "")
        department = body.get('department', "")
        role = body.get('role', "")
        employee = Employee.objects.filter(username=user_id)
        
        if len(employee) != 1:
            employee = Employee(username=user_id)
        else:
            employee = employee[0]
        employee.gender = gender
        employee.department = Department.objects.get(pk=department)
        employee.role = Role.objects.get(pk=role)

        # allowing employee access to the website
        employee.is_staff = True
        employee.save()
        if employee.gender == 'M' or employee.gender == 'P':
            Leave(employee=employee, maternity_d=0).save()
        else:
            Leave(employee=employee).save()
            
        return JsonResponse({"message": "Finished"})

@login_required
def add_department(request):
    if request.method == "POST":
        body = json.loads(request.body)
        department_name = body.get('name', "")

        department = Department.objects.filter(name=department_name)

        # Preventing department duplicates
        if len(department) != 0:
            return JsonResponse({"message": "Department Exists"})
        
        Department(name=department_name).save()
        return JsonResponse({'message': 'Department Added'})

@login_required
def request_leave(request):
    employee = Employee.objects.get(pk=request.user.id)
    if request.method =="POST":
        # storing all the input fields        
        leaveType = request.POST['leave-type']
        days = request.POST['days']
        hours = request.POST['hours']
        startDate = request.POST['start-date']
        endDate = request.POST['end-date']
        startTime = request.POST['start-time']
        endTime = request.POST['end-time']
        reason = request.POST['reason'] 
        leavePay = request.POST['leave-pay']    
        
        if leavePay == 'To be decided by the management':
            leavePay = ""

        if bool(request.FILES.get('files', False)) == False:
            Request(employee=employee, leave=leaveType, days=float(days),
                hours=hours, startDate=startDate, endDate=endDate,
                payment=leavePay, startTime=startTime, endTime=endTime, 
                reason=reason
            ).save()    
        else:
            Request(employee=employee, leave=leaveType, days=float(days),
                hours=hours, startDate=startDate, endDate=endDate,
                payment=leavePay, startTime=startTime, endTime=endTime, 
                reason=reason, document=request.FILES['files']
            ).save()
        return JsonResponse({"message": "success"})
    elif request.method == "GET":
        if not get_referer(request):
            raise Http404

        delegate = Delegate.objects.get(employee=employee)
        if employee.role.name == "CEO":
            leave_requests = Request.objects.filter(decision="Pending").exclude(employee=employee)
        elif employee.department.name == "People and Culture" and employee.role.name == "Head of Department":
            leave_requests = Request.objects.filter(decision="Pending").exclude(employee=employee)
        elif employee.department.name == "People and Culture":
            role = Role.objects.get(name='Head of Department')
            # exclude the head
            head = Employee.objects.get(department=employee.department, role=role)
            leave_requests = Request.objects.filter(decision="Pending").exclude(Q (employee=employee) & Q(employee=head))
        elif employee.role.name == "Head of Department":
            leave_requests = Request.objects.filter(employee__department=employee.department, decision="Pending").exclude(employee=employee)
        
        elif delegate.status == "Active": #LMS
            delegated_by = delegate.delegated_by #Nabeel

            if delegated_by.role.name == "CEO":
                leave_requests = Request.objects.filter(decision="Pending").exclude(Q(employee=delegated_by) & Q(employee=employee))
            elif delegated_by.department.name == "People and Culture" and delegated_by.role.name == "Head of Department":
                leave_requests = Request.objects.filter(decision="Pending").exclude(Q(employee=delegated_by) & Q(employee=employee))
            elif delegated_by.department.name == "People and Culture":
                role = Role.objects.get(name='Head of Department')
                # exclude the head
                head = Employee.objects.get(department=delegated_by.department, role=role)
                leave_requests = Request.objects.filter(decision="Pending").exclude(Q (employee=delegated_by) & Q(employee=head) & Q(employee=employee))
            elif delegated_by.role.name == "Head of Department":
                leave_requests = Request.objects.filter(employee__department=delegated_by.department, decision="Pending").exclude(Q(employee=delegated_by) & Q(employee=employee))
        else:
            return JsonResponse([], safe=False)    
        return JsonResponse([leave_request.serialize() for leave_request in leave_requests], safe=False)
        
@login_required
def leaves_list(request, username=""):
    if username == "":
        employee = Employee.objects.get(pk=request.user.id)
    else:
        employee = Employee.objects.get(username=username)
    if request.method == "GET":
        if not get_referer(request):
            raise Http404
        return JsonResponse(Leave.objects.get(employee=employee).serialize())
    
    # updating the leaves of the user
    elif request.method == "PUT":
        body = json.loads(request.body)
        leave_type = body.get('add-leave-type', "")
        days = body.get('leave-days', "")
        reason = body.get('reason', "")
        hours = body.get('leave-hours', "")

        leave = Leave.objects.get(employee=employee)

        modifier = Employee.objects.get(pk=request.user.id)
        
        if leave_type == 'short_h':
            # modifying the value of Days in Lieu attribute
            setattr(leave, leave_type, getattr(leave, leave_type) + int(hours))
            leave.save()

            Modification(employee=employee, modifier=modifier, hours=hours, leave=leave_type, reason=reason).save()
        else:
            # modifying the value of Days in Lieu attribute
            setattr(leave, leave_type, getattr(leave, leave_type) + Decimal(days))
            leave.save()

            Modification(employee=employee, modifier=modifier, days=days, leave=leave_type, reason=reason).save()
        
        return JsonResponse({'message': "Leave Added"})

@login_required
def current_employee(request):
    employee = Employee.objects.get(username=request.user.username)
    # get the current employee selected
    if request.method == "GET":
        if not get_referer(request):
            raise Http404
        return JsonResponse(employee.serialize())
    elif request.method == "PUT":
        data = json.loads(request.body)
        gender = data.get('gender', "")
        employee.gender = gender
        employee.save()

        leave = Leave.objects.get(employee=employee)

        if employee.gender == "M" or employee.gender == "P":
            leave.maternity_d = 0
            leave.save()
        return JsonResponse(employee.serialize())

@login_required
def employees_list(request):
    employee = Employee.objects.get(pk=request.user.id)
    # List all employees in the currently logged in user's department and exclude the head of department
    if employee.role.name == "CEO":
        employees_list = Employee.objects.all().exclude(username=employee.username)
    elif employee.department.name == "People and Culture" and employee.role.name == "Head of Department":
        employees_list = Employee.objects.all().exclude(username=employee.username)
    elif employee.department.name == "People and Culture":
        role = Role.objects.get(name='Head of Department')
        # exclude the head
        head = Employee.objects.get(department=employee.department, role=role)
        employees_list = Employee.objects.all().exclude(Q (username=employee.username) & Q(username=head.username))
    elif employee.role.name == "Head of Department":
        employees_list = Employee.objects.filter(department=employee.department).exclude(username=employee.username)

    return JsonResponse([employee.serialize() for employee in employees_list], safe=False)

@login_required
def leave_requests(request):
    if request.method == "GET":
        if not get_referer(request):
            raise Http404
        employee = Employee.objects.get(pk=request.user.id)
        department = Department.objects.get(name=employee.department)
        leave_requests = Request.objects.filter(employee__department=department, decision="Pending").exclude(employee=employee).order_by('-id')
        return JsonResponse([leave_request.serialize() for leave_request in leave_requests], safe=False)

@login_required
def request_details(request, requestID):
    leave_request = Request.objects.get(pk=requestID)
    if request.method == "GET":
        if not get_referer(request):
            raise Http404
        return JsonResponse(leave_request.serialize())
    elif request.method == "PUT":
        body = json.loads(request.body)
        decision = body.get('decision', "")  
        payment = body.get('payment', "")
        comment = body.get('comment', "")
        leave_request.decision = decision
        leave_request.payment = payment
        leave_request.comment = comment
        leave_request.save()

        if decision == "Approved":
            employee = leave_request.employee
            leave_type = leave_request.leave
            days = leave_request.days
            hours = leave_request.hours
            leave = Leave.objects.get(employee=employee)

            if leave_type != 'unpaid_d' and leave_type != 'other_d':
                if leave_type == 'short_h':
                    setattr(leave, leave_type, getattr(leave, leave_type) - hours)
                elif leave_type == 'religious_d':
                    remaining_days = getattr(leave, leave_type)
                    if remaining_days >= days:
                        setattr(leave, leave_type, remaining_days - days)
                    elif remaining_days < days: 
                        extra = days - remaining_days 
                        if remaining_days > 0:
                            setattr(leave, leave_type, days - remaining_days)
                        leave.annual_d = leave.annual_d - extra
                else:
                    setattr(leave, leave_type, getattr(leave, leave_type) - days)    
                leave.save()
        return JsonResponse({'message': "Success"})

@login_required
def past_leaves(request, username=""):
    if username == "":
        employee = Employee.objects.get(pk=request.user.id)
    else:
        employee = Employee.objects.get(username=username)
    if request.method == "GET":
        if not get_referer(request):
            raise Http404
        leave_requests = Request.objects.filter(employee=employee, decision='Approved').order_by('-id')
        return JsonResponse([leave_request.serialize() for leave_request in leave_requests], safe=False)

@login_required
def past_requests(request, username=""):
    if username == "":
        employee = Employee.objects.get(pk=request.user.id)
    else:
        employee = Employee.objects.get(username=username)

    leave_requests = Request.objects.filter(Q(decision="Rejected") | Q(decision="Approved"), employee=employee).order_by('-id')

    if request.method == "GET":
        if not get_referer(request):
            raise Http404
        return JsonResponse([leave_request.serialize() for leave_request in leave_requests], safe=False)         

@login_required    
def pending_requests(request, username=""):
    if username == "":
        employee = Employee.objects.get(pk=request.user.id)
    else:
        employee = Employee.objects.get(username=username)

    leave_requests = Request.objects.filter(Q(decision="Pending"), employee=employee).order_by('-id')

    if request.method == "GET":
        if not get_referer(request):
            raise Http404
        return JsonResponse([leave_request.serialize() for leave_request in leave_requests], safe=False)    

@login_required
def delete_requests(request, requestID):
    if request.method == "DELETE":
        req = Request.objects.get(pk=requestID)

        if req.decision == 'Approved':
            leave = Leave.objects.get(employee=req.employee)
            if (req.leave != 'other_d' and req.leave != 'unpaid_d'):
                setattr(leave, req.leave, getattr(leave, req.leave) + int(req.days))
                leave.save()

        Request.objects.get(pk=requestID).delete()
        return JsonResponse({'message': "Request deleted."})

@login_required
def modify_requests(request, requestID):
    if request.method == "POST":
        
        # storing all the input fields        
        leaveType = request.POST['leave-type']
        days = request.POST['days']
        hours = request.POST['hours']
        startDate = request.POST['start-date']
        endDate = request.POST['end-date']
        startTime = request.POST['start-time']
        endTime = request.POST['end-time']
        reason = request.POST['reason'] 
        leavePay = request.POST['leave-pay'] 

        leave_request = Request.objects.get(pk=requestID)
        leave_request.leave = leaveType
        leave_request.days = Decimal(days)
        leave_request.hours = int(hours)
        leave_request.startDate = startDate
        leave_request.endDate = endDate
        leave_request.startTime = startTime
        leave_request.endTime = endTime
        leave_request.reason = reason
        leave_request.payment = leavePay

        if bool(request.FILES.get('files', False)) == True:
            document = request.FILES['files']
            leave_request.document = document

        leave_request.save()

        return JsonResponse({'message': "Leave Modified"})

@login_required
def add_study_period(request):
    if request.method == "PUT":
        body = json.loads(request.body)
        start_date = body.get('start_date', "")
        end_date = body.get('end_date', "")
        department = body.get('department', "")
    
        dept = Department.objects.get(name=department)
        
        dept.startDate = start_date
        dept.endDate = end_date
        
        dept.save()
        return JsonResponse({'message': 'Study Period Added'}) 

@login_required
def delegate_employee(request):
    if request.method == "POST":
        delegator = Employee.objects.get(pk=request.user.id)
        #department = Department.objects.get(name=employee.department)
        data = json.loads(request.body)
        employee_ID = data.get('employee')
        startDate = data.get('start-date')
        endDate = data.get('end-date')

        delegated_employee = Employee.objects.get(username=employee_ID)

        if timeDiff(startDate, endDate):
            status = 'Active'
        else:
            status = 'Inactive'

        delegate = Delegate.objects.get(employee=delegated_employee)
        delegate.startDate = startDate
        delegate.endDate = endDate
        delegate.status = status
        delegate.delegated_by = delegator
        delegate.save()

        return JsonResponse({"message": "Delegated"})

@login_required
def view_department(request):
    if request.method == "GET":
        if not get_referer(request):
            raise Http404
        employee = Employee.objects.get(pk=request.user.id)
        return JsonResponse(Department.objects.get(name=employee.department).serialize())

@login_required
def carry_forward_leaves(request):
    if request.method == "PUT":
        data = json.loads(request.body)
        employee_ID = data.get('employee', "")
        carry_forward = bool(data.get('carry-forward', ""))

        employee = Employee.objects.get(username=employee_ID)
        leave = Leave.objects.get(employee=employee)
        leave.annual_carry_forward = carry_forward
        leave.save()

        return JsonResponse({"message": "Saved carry forward setting."})

@login_required
def department_head(request, dept_id):
    department = Department.objects.get(id=dept_id)
    role = Role.objects.get(name='Head of Department')
    employees = Employee.objects.filter(Q(department=department) & Q(role=role))

    return JsonResponse([employee.serialize() for employee in employees], safe=False)

@login_required
def ceo_checker(request):
    role = Role.objects.get(name='CEO')
    employees = Employee.objects.filter(role=role)

    return JsonResponse([employee.serialize() for employee in employees], safe=False)

@login_required
def mod_check(request, modID):
    if request.method == "GET":
        if not get_referer(request):
            raise Http404

        mod = Modification.objects.get(pk=modID)
        return JsonResponse(Modification.objects.get(pk=modID).serialize())

@login_required
def add_holidays(request):
    if request.method == "POST":
        body = json.loads(request.body)
        holidayName = body.get('holiday-name', "")
        holidayDate = body.get('holiday-date', "")
        holidayEndDate = body.get('holiday-end-date', "")
        
        obj = Holiday.objects.filter(holidayDate=holidayDate)
        if len(obj) != 0:
            return JsonResponse({'message': 'Holiday Exists!'})
        
        Holiday(holidayName=holidayName, holidayDate=holidayDate, holidayEndDate=holidayEndDate).save()
        return JsonResponse({'message': 'Holiday Added'})
    
    if request.method == "GET":
        if not get_referer(request):
            raise Http404

        holidays = Holiday.objects.all()
        return JsonResponse([holiday.serialize() for holiday in holidays], safe=False)

@login_required
def remove_holiday(request):
    if request.method == "POST":
        body = json.loads(request.body)
        holiday_id = body.get('id', "")
        
        Holiday.objects.get(id=holiday_id).delete()
        return JsonResponse({'message': 'Holiday Deleted'})