from .models import *
from datetime import datetime

def timeDiff(start, end):
    start = datetime.strptime(start, "%Y-%m-%d")
    start = start.strftime("%d-%m-%y")

    current = datetime.now().strftime("%d-%m-%y")
    end = datetime.strptime(end, "%Y-%m-%d")
    end = end.strftime("%d-%m-%y")

    if start <= current <= end:
        return True
    return False

def dateDiff(start):
    current = datetime.now().date()
    num_months = (current.year - start.year) * 12 + (current.month - start.month)

    if num_months < 6:
        return True
    return False

# prevening from accessing API links directly
def get_referer(request):
    referer = request.META.get('HTTP_REFERER')
    if not referer:
        return None
    return referer