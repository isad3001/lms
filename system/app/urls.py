from django.contrib import admin
from django.urls import path
from .views import *

urlpatterns = [
    path('login', login_view, name='login'),
    path('logout', logout_view, name='logout'),
    path('', index, name='index'),
    path('addEmployee', add_employee, name='add_employee'),
    path('addDepartment', add_department, name='add_department'),
    path('leaves', leaves_list, name='leaves'),
    path('leaves/<str:username>', leaves_list, name='leaves_list'), #Days in Lieu are added via this link
    path('requests/<str:requestID>', request_details, name='request_details'),
    path('requests', request_leave, name='request_leave'),
    path('employee', current_employee, name='employee'),
    path('employeesList', employees_list, name='employees_list'),
    path('pastLeaves', past_leaves),
    path('pastLeaves/<str:username>', past_leaves),
    path('pastRequests/', past_requests),
    path('pastRequests/<str:username>', past_requests),
    path('pendingRequests/', pending_requests),
    path('pendingRequests/<str:username>', pending_requests),
    path('deleteRequests/<str:requestID>', delete_requests),
    path('modifyLeaves/<str:requestID>', modify_requests),
    path('addStudyPeriod', add_study_period),
    path('delegate', delegate_employee),
    path('department', view_department),
    path('carryForwardLeaves', carry_forward_leaves),
    path('departmentHead/<int:dept_id>', department_head),
    path('modifications/<str:modID>', mod_check),
    path('holidays', add_holidays),
    path('removeholiday', remove_holiday),
    path('CEOChecker', ceo_checker)
]
