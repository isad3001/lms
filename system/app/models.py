from django.db import models
from django.contrib.auth.models import AbstractUser
from django.contrib.postgres.fields import ArrayField
import django
from django.db.models.fields import DateField

class Department(models.Model):
    name = models.CharField(max_length=200, default='', unique=True)
    startDate = models.CharField(max_length=20, blank=True, null=True)
    endDate = models.CharField(max_length=20, blank=True, null=True)

    def __str__(self):
        return self.name

    def serialize(self):
        return {
            "name": self.name,
            'startDate': self.startDate,
            'endDate': self.endDate
        }

class Role(models.Model):
    name = models.CharField(max_length=200, default='', unique=True)

    def __str__(self):
        return self.name

    def serialize(self):
        return {
            "name": self.name
        }

class Delegate(models.Model):
    STATUS = [
        ('Active', 'Active'),
        ('Inactive', 'Inactive')    
    ]
    employee = models.ForeignKey('Employee', on_delete=models.CASCADE, null=False, default='', related_name='delegated')
    startDate = models.CharField(max_length=15, blank=True, null=True)
    endDate = models.CharField(max_length=15, blank=True, null=True)          
    status = models.CharField(max_length=10, choices=STATUS, default='Inactive')
    delegated_by = models.ForeignKey('Employee', on_delete=models.CASCADE, null=True, related_name="delegated_by") 

    def __str__(self):
        return f"{self.status}"

    def serialize(self):
        return {
            "startDate": self.startDate,
            "endDate": self.endDate,
            "status": self.status,
            "delegated_by": self.delegated_by.username
        }

class Employee(AbstractUser):
    GENDER = [
        ('M', 'Male'),
        ('F', 'Female'),
        ('P', 'Prefer not to Say')
    ]
    gender = models.CharField(max_length=3, default='')
    department = models.ForeignKey(Department, on_delete=models.CASCADE, blank=True, null=True)
    role = models.ForeignKey(Role, on_delete=models.CASCADE, null=True, blank=True)
    date = models.CharField(max_length=40, blank=True, null=True)

    def serialize(self):
        serialized_value = {
                "employee": self.username,
                "firstname": self.first_name,
                "lastname": self.last_name,
                "gender": self.gender,
                "department": self.department.name,
                "date": self.date,
            } 
        if self.role is not None:
            serialized_value['role'] = self.role.name             
             
        return serialized_value

        
class Leave(models.Model):
    employee = models.ForeignKey(Employee, on_delete=models.CASCADE)
    annual_d = models.IntegerField(default=22)
    sick_d = models.DecimalField(max_digits=4, decimal_places=1, default=15.0,blank=True, null=True)
    maternity_d = models.IntegerField(default=45, blank=True, null=True)
    parental_care_d = models.IntegerField(default=5, blank=True, null=True) 
    hajj_d = models.IntegerField(default=30, blank=True, null=True)
    short_h = models.IntegerField(default=36, blank=True, null=True)
    religious_d = models.IntegerField(default=1, blank=True, null=True)
    bereavement_d = models.IntegerField(default=5, blank=True, null=True)
    days_in_lieu_d = models.IntegerField(default=0,blank=True, null=True)
    annual_carry_forward = models.BooleanField(default=False)
    def serialize(self):
        return {
            "employee": self.employee.username,
            "name": f"{self.employee.first_name} {self.employee.last_name}",
            "annual_d": self.annual_d,
            "sick_d": self.sick_d,
            "maternity_d": self.maternity_d,
            "parental_care_d": self.parental_care_d,
            "hajj_d": self.hajj_d,
            "short_h": self.short_h,
            "religious_d": self.religious_d,
            "bereavement_d": self.bereavement_d,
            "days_in_lieu_d": self.days_in_lieu_d,
            "annual_carry_forward": self.annual_carry_forward
        }

class Request(models.Model):
    LEAVES = [
        ('annual_d', 'Annual'),
        ('sick_d', 'Sick'),
        ('maternity_d', 'Maternity'), 
        ('parental_care_d', 'Parental Care'),
        ('hajj_d', 'Hajj'), 
        ('short_h', 'Short'), 
        ('religious_d', 'Religious Half-Day'),
        ('bereavement_d', 'Bereavement'),
        ('unpaid_d', 'Unpaid'),
        ('days_in_lieu_d', 'Days in Lieu'), 
        ('other_d', 'Other')
    ]

    PAYMENT = [
        ('Paid', 'Paid'),
        ('Unpaid', 'Unpaid'),
        ('Half-Pay', 'Half-Pay')
    ]

    DECISION = [
        ('Pending', 'Pending'),
        ('Approved', 'Approved'),
        ('Rejected', 'Rejected')
    ]
    # Requesting employee
    employee = models.ForeignKey(Employee, on_delete=models.CASCADE, blank=False, null=True)
    leave = models.CharField(choices=LEAVES, max_length=200, blank=False, null=False)
    reason = models.CharField(max_length=500, blank=True, null=True)
    days = models.DecimalField(max_digits=4, decimal_places=1, blank=False, null=False)
    hours = models.IntegerField(default=0, blank=True, null=True)
    startDate = models.CharField(max_length=20, blank=False, null=False)
    endDate = models.CharField(max_length=20, blank=False, null=False)
    document = models.FileField(blank=True, null=True)
    payment = models.CharField(choices=PAYMENT, max_length=200, blank=True, null=True)
    startTime = models.CharField(max_length=20, blank=True, null=True)
    endTime = models.CharField(max_length=20, blank=True, null=True)
    decision = models.CharField(max_length=10, choices=DECISION, default='Pending')
    comment = models.CharField(max_length=500, blank=True)

    def serialize(self):
        return {
            "requestID": self.id,
            "employee": self.employee.username,
            "name": f"{self.employee.first_name} {self.employee.last_name}",
            "leave": self.leave,
            "reason": self.reason,
            "days": self.days,
            "hours": self.hours,
            "startDate": self.startDate,
            "endDate": self.endDate,
            "document": self.document.name,
            "payment": self.payment,
            "startTime": self.startTime,
            "endTime": self.endTime,
            "decision": self.decision,
            "comment": self.comment
        }

    def __str__(self):
        return f"{self.employee.username}, {self.leave}, {self.days}, {self.hours}"

class Modification(models.Model):
    employee = models.ForeignKey(Employee, on_delete=models.CASCADE, related_name="employee")
    modifier = models.ForeignKey(Employee, on_delete=models.CASCADE, related_name="modifier")
    leave = models.CharField(max_length=200, blank=False, null=False)
    days = models.DecimalField(max_digits=4, decimal_places=1, blank=True, null=True)
    hours = models.IntegerField(blank = True, null=True)
    reason = models.CharField(max_length=250, blank=False, null=False)
    lieu_counter = models.DateField(default=django.utils.timezone.now, blank=True, null=True)
    lieu_check = models.CharField(default="untouched", max_length=20, blank=True, null=True)

    def serialize(self):
        return {
            "modificationID": self.id,
            "employee": self.employee.username,
            "modifier": self.modifier.username,
            "leave": self.leave,
            "days": self.days,
            "hours": self.hours,
            "reason": self.reason,
            "dateAdded": self.lieu_counter,
            "lieuCheck": self.lieu_check
        }
    def __str__(self):
        return f"{self.employee.username}, {self.modifier.username}, {self.leave}, {self.days}, {self.hours}, {self.reason}, {self.lieu_counter}, {self.lieu_check}"


class Holiday(models.Model):
    holidayName = models.CharField(blank=False, null = False, max_length=50)
    holidayDate = models.CharField(blank=False, null=False, max_length=20)
    holidayEndDate = models.CharField(blank=True, null=True, max_length=20)

    def serialize(self):
        return {
            "holidayID": self.id,
            "holidayName": self.holidayName,
            "holidayDate": self.holidayDate,
            "holidayEndDate": self.holidayEndDate
        }

    def __str__(self):
        return f"{self.holidayName}, {self.holidayDate}, {self.holidayEndDate}"
