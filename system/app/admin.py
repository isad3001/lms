from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.forms import UserCreationForm, UserChangeForm
from .models import *
# Register your models here.

class CustomUserAdmin(UserAdmin):
    model = Employee
    add_form = UserCreationForm
    form = UserChangeForm
    list_display = ['pk', 'email', 'username', 'first_name', 'last_name', 'gender', 'date', 'department', 'role']
    add_fieldsets = UserAdmin.add_fieldsets + (
        (None, {'fields': ('email', 'first_name', 'last_name', 'gender', 'date', 'department', 'role')}),
    )
    fieldsets = UserAdmin.fieldsets

admin.site.register(Employee, CustomUserAdmin)
admin.site.register(Department)
admin.site.register(Leave)
admin.site.register(Request)
admin.site.register(Delegate)
admin.site.register(Holiday)
admin.site.register(Modification)
admin.site.register(Role)
