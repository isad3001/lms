# LEAVE MANAGEMENT SYSTEM #

A leave management system is a system that will allow users to manage their leaves that they
may be eligible for during their employment at TAG.

The system will be designed in a way that will allow users to accomplish the following sets
of goals:

1. A centralized system that contains the information of all the employees regarding their leaves in one place.

2. Allow the organization to track the leaves of each employee.


3. Allow employees to request leaves instantly through the system.

4. Allow P&C to add days-in-lieu, add annual leave, return unused annual leave (annual leave scheduled but not taken), add holidays into the system, and to extend or cancel unused annual leave at year end.

5. Allow P&C to add departments, assign department heads, add employees, and assign employees to departments.

6. Allow employees to withdraw or change a requested leave.

7. Allow managers to cancel a leave that has already been approved.

8. Allow the system to maintain audit trails.

### Running the Application ###

Requirements:

1. Docker

2. VPN (To connect to TAG's network)


Steps to run the application:

1. Go to the Dockerfile/docker-compose.yaml file directory.

2. Build the docker container

	command: ` docker-compose build `
	
	
3. Run the docker

	command: ` docker-compose up `	
	

4. Open 127.0.0.1:8000 on a web browser to use the application.


Production Environment

1. Settings.py contains a secret key which should not be added to the repository for the public to see, so a secret.txt is
created which stores the key. A unique key can be generated through this link: https://djecrety.ir/

2. The application has the following servers running:

	- Redis
	- Celery Worker
	- Celery Beat
	- ElephantSQL (Database Server)
	- Django Server
	- File Server

3. File server should be added to store the supporting documents. After the file server is configured. The DEBUG variable in settings.py should be changed to False when the application is running in a production environment. 

### Repository Management ###

This repository is currently managed by two people. 

Ibad Altaf - 20018177@student.curtin.edu.au

Syed Nabeel - 20002471@student.curtin.edu.au

For more information about the application, a manual will be provided during the Handover.