FROM python:3.9.4
ENV PYTHONUNBUFFERED=1
WORKDIR /lms
COPY requirements.txt /lms
RUN apt-get update && apt-get install -y python-dev libldap2-dev libsasl2-dev libssl-dev
RUN pip install -r requirements.txt
COPY . /lms
WORKDIR /lms/system
EXPOSE 8000